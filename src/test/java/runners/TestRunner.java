package runners;

import java.io.File;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import com.intuit.karate.KarateOptions;
//import org.apache.commons.io.FileUtils;
import com.intuit.karate.Runner;
import com.intuit.karate.Results;

import static org.junit.Assert.assertTrue;
//import static org.junit.jupiter.api.Assertions.*;

import org.junit.Test;
//
//import net.masterthought.cucumber.Configuration;
//import net.masterthought.cucumber.ReportBuilder;

@KarateOptions(features = "classpath:features", tags = "~@ignore")
public class TestRunner {

    @Test
    public void test() {
        Results results = Runner.parallel(getClass(), 1, "target");
        assertTrue(results.getErrorMessages(), results.getFailCount() == 0);
    }
//    public static void generateReport(String karateOutputPath) {
//        Collection<File> jsonFiles = FileUtils.listFiles(new File(karateOutputPath), new String[] {"json"}, true);
//        List<String> jsonPaths = new ArrayList<String>(jsonFiles.size());
//        jsonFiles.forEach(file -> jsonPaths.add(file.getAbsolutePath()));
//        Configuration config = new Configuration(new File("target"), "Test Automation");
//        ReportBuilder reportBuilder = new ReportBuilder(jsonPaths, config);
//        reportBuilder.generateReports();
//    }
}
