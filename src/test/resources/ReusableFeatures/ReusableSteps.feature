@ignore
Feature: Reusable Steps


  @Create
  Scenario Outline: Create Issues

    Given url baseURL + '/<userId>/issues'
    And header Authorization = 'Bearer ' + token
    And request { title: '#(CRUDActionsCreateScenario)' }
    When method POST

    #API Response Validation:
    Then status <code>
    * def id = response.iid
    And print id

    Examples:
      | userId   | code |
      | 35387680 | 201  |
