Feature: CRUD Operations in Issues

  #Call the Commom Post Request Save the iid in cache and use it for other RUD Operations.
  Background:
    * def issue = callonce read('classpath:ReusableFeatures/ReusableSteps.feature@Create')
    * def iid = issue.id

  Scenario Outline: Get Issues
    Given url baseURL + '/<userId>/issues/' + iid
    And header Authorization = 'Bearer ' + token
    When method GET
    #API Response Validation:
    Then status <code>
    And match response.title == "<title>"

    Examples:
      | userId   | code | title                     |
      | 35387680 | 200  | CRUDActionsCreateScenario |

  Scenario Outline: Update Issues
    Given url baseURL + '/<userId>/issues/' +iid
    And header Authorization = 'Bearer ' + token
    #Upadte the Issue Title
    And request { title: 'CRUDActionsUpdateScenario' }
    When method PUT
    #API Response Validation:
    Then status <code>
    And match response.title == "<title>"

    Examples:
      | userId   | code | title                     |
      | 35387680 | 200  | CRUDActionsUpdateScenario |

  Scenario Outline: Delete Issues
    Given url baseURL + '/<userId>/issues/' +iid
    And header Authorization = 'Bearer ' + token
    When method Delete
    #API Response Validation:
    Then status <code>
    Examples:
      | userId   | code |
      | 35387680 | 204  |