Feature: Create Issues


  Scenario Outline: Create Issues - Multiple Issues creation in a Single Feature file.
    #Request Body can be configured as placeholders or directly as json structure or even through BDD Tables.
    * def title = '<title>'
    * def description = '<description>'
    * def assigneeId = '<assigneeId>'
    * def confidential = '<confidential>'
    * def createdOn = '<createdOn>'
    * def discussionToResolve = '<discussionToResolve>'
    * def dueDate = '<dueDate>'
    * def issueType = '<issueType>'
    * def labels = '<labels>'
    * def milestoneId = '<milestoneId>'
    * def weight = '<weight>'
    * def payload = read("classpath:data/Create.json")

    Given url baseURL + '/<userId>/issues'
    And header Authorization = 'Bearer ' + token
    And request payload
    When method POST
    #API Response Validation:
    Then status <code>
    And match response.title == "<title>"
    And match response.description == "<description>"
    And match response.description == "<description>"
    And match response.state == "<state>"
    And match response.author.state == "<authorState>"
    #Similarly all the response parameter can be validated using the parametrization used in Example section.

    Examples:
      | userId   | code | title            | description            | assigneeId | confidential | createdOn            | discussionToResolve | dueDate    | issueType | labels  | milestoneId | weight | state  | authorState |
      | 35387680 | 201  | CreateNewIssue#1 | DescriptionForNewIssue | 1          | true         | 2022-01-02T03:45:40Z | 1                   | 2022-12-12 | issue     | label#1 | 12          | 3      | opened | active      |
      | 35387680 | 201  | CreateNewIssue#2 | DescriptionForNewIssue | 1          | false        | 2022-03-02T03:45:40Z | 1                   | 2022-11-12 | incident  | label#2 | 11          | 2      | opened | active      |
      | 35387680 | 201  | CreateNewIssue#3 | DescriptionForNewIssue | 1          | true         | 2022-04-02T03:45:40Z | 1                   | 2022-10-12 | test_case | label## | 10          | 1      | opened | active      |


  Scenario Outline: Edge Case Scenario - when the User is UnAuthorized
    Given url baseURL + '/<userId>/issues'
    #Pass an Invalid Token
    And header Authorization = 'Bearer ' + invalidToken
    And request { title: 'CRUDActionsCreateScenario' }
    When method POST
    #API Response Validation:
    Then status <code>
    And match response.message == "401 Unauthorized"
    Examples:
      | userId   | code |
      | 35387680 | 401  |


  Scenario Outline: Create Issues - When Few Parameters in request json are incorrect
    Given url baseURL + '/<userId>/issues'
    And header Authorization = 'Bearer ' + token
    #Do not pass title field in Json
    #Pass String in Integer IID parameter
    #Pass Incorrect IssueType
    And request { iid: '<iid>', issue_type: '<issueType>' }
    When method POST
    Then status <code>
    #These Test Situations can be validated separately also
    And match response.error == "title is missing, iid is invalid, issue_type does not have a valid value"

    Examples:
      | userId   | code | iid | issueType |
      | 35387680 | 400  | ABC | feature   |