Feature: Edge Cases in Issues

    #Call the Commom Post Request Save the iid in cache and use it for other RUD Operations.
  Background:

    * def issue = callonce read('classpath:ReusableFeatures/ReusableSteps.feature@Create')
    * def iid = issue.id

  Scenario Outline: Edge Case Scenario - Duplicate Issue
    Given url baseURL + '/<userId>/issues/'
    And header Authorization = 'Bearer ' + token
    #Create a POST request with same IID
    And request { title: 'CRUDActionsCreateScenario', iid: "#(iid)"}
    When method POST
    Then status <PostDuplicateCode>
    And match response.message == "Duplicated issue"

    Examples:
      | userId   | PostDuplicateCode |
      | 35387680 | 409               |

  Scenario Outline: Edge Case Scenario - Add a To-Do Issue
    Given url baseURL + '/<userId>/issues/' + iid + '/todo'
    And header Authorization = 'Bearer ' + token
    And request { iid: "#(iid)"}
#    Create a to-do POST request with same IID
    When method POST
    Then status <PostCode>
    #In the Response the target is set as Open and teh Action Name is Marked
    And match response.target.state == 'opened'
    And match response.action_name == 'marked'

    Examples:
      | userId   | PostCode |
      | 35387680 | 201      |

  Scenario Outline: Edge Case Scenario - Setting Time Tracking Status
    Given url baseURL + '/<userId>/issues/' + iid + '/time_estimate'
    And header Authorization = 'Bearer ' + token
    And request { iid: "#(iid)", duration: '<duration>'}
#    Create a to-do POST request with same IID
    When method POST
    Then status <PostCode>
#    In the Response the target is set as Open and teh Action Name is Marked
    And match response.human_time_estimate == '<setDuration>'
    #Calculate the time in seconds and compare the result with the response from the response.
    * def timeEstimate = response.time_estimate
    * def functions = Java.type('utilities.customFunctions')
    * def timeConversions = functions.conversion(4)
    # Comparing the calculated time and the time in response
    And match timeConversions == timeEstimate

    #Reset the time_estimate to 0 seconds
    Given url baseURL + '/<userId>/issues/' + iid + '/reset_time_estimate'
    And header Authorization = 'Bearer ' + token
    And request { iid: "#(iid)", duration: '<duration>'}
#    Create a to-do POST request with same IID
    When method POST
    Then status <PostCode>
    And match response.time_estimate == 0
    Examples:
      | userId   | PostCode | duration | setDuration |
      | 35387680 | 200      | 4h       | 4h          |

  Scenario Outline: Edge Case Scenario - Search for a Deleted Issue
    #Create an Issue, Get the Issue
    Given url baseURL + '/<userId>/issues/' + iid
    And header Authorization = 'Bearer ' + token
    When method GET
    #API Response Validation:
    Then status <GetCode>
    #Delete the Issue
    Given url baseURL + '/<userId>/issues/' +iid
    And header Authorization = 'Bearer ' + token
    When method Delete
    #API Response Validation:
    Then status <DeleteCode>
    #Try to Get the same Issue which was Deleted in previous step
    Given url baseURL + '/<userId>/issues/' + iid
    And header Authorization = 'Bearer ' + token
    When method GET
    Then status <IssueNotFoundCode>
    And match response.message == "404 Not found"

    Examples:
      | userId   | GetCode | DeleteCode | IssueNotFoundCode |
      | 35387680 | 200     | 204        | 404               |

