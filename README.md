## Why Did I Use Karate Framework
Karate has a list of Advantages and Conveniences when it comes to complete/pure Backend Testing with REST API.
Quick and Easy to Install, Configure and Design.
When we are moving towards more of Script-less Automation this framework would be ideal for Test Automation experts to configure.
By Using this framework dependency over using a specific technology for Test Automation can be overcome such that non-Java skilled people could also use and benefit out of this.
Its an One Stop-Open Source Framework - which offers in-built report generations as well.
No vulnerabilities identified found.

## How to execute the Tests
To execute all the Features together we could use mvn clean test
Or tags can be used to Execute specific tests.

##CRUD Operations
Karate supports the concept of involving all CRUD Operations in a single Scenario, as its considered as one flow but in this framework I have separated them as different scenarios by keeping the Create Operation as an One-Time call. 

##Issues Edge Cases
The following Scenarios were tested
UnAuthorized User is Not Allowed to Create an Issue.
Deleted Issue cannot be retrieved.
Duplicate Issue cannot be created.
Adding Time Estimate by invoking java utils.
Adding a To-Do to an Issue.


## Improvements that can be made
Date Values date can be configured to System date.
Move test data creation to a common file.
Configure Performance Testing using Gatling
Call Scenarios across within same Feature file, will be a part of Karate 1.2.0
